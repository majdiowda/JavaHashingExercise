import java.security.MessageDigest;
import java.io.FileInputStream;
 
public class MD5HashingExample 
{
    public static void main(String[] args) throws Exception
    {

        MessageDigest md = MessageDigest.getInstance("MD5");
        // SHA-256 or MD5 or SHA-1
        
        String password = "test123";
        
        md.update(password.getBytes());
 
        byte byteData[] = md.digest();
        
        StringBuffer hexString = new StringBuffer();
		
    	for (int i=0;i<byteData.length;i++) {
    		String hex=Integer.toHexString( byteData[i] & 0xff );
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
    	System.out.println("Digest(in hex format) for: "+password +" - "+ hexString.toString());
    }
	// Add Methods for MD5, SHA-256 and SHA-1
}